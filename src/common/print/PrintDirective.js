(function() {
  var module = angular.module('loom_print_directive', []);

  module.directive('loomPrint',
      function($rootScope, $translate, $http, $q, dialogService, $timeout, mapService, configService, pulldownService) {
        return {
          restrict: 'C',
          replace: true,
          templateUrl: 'print/partial/print.tpl.html',
          // The linking function will add behavior to the template
          link: function(scope) {
            $(window).resize(function() {
              drawBox();
            });
            // Disable cache for getJSON request
            $.ajaxSetup({ cache: false });
            var getAppRequest = configService.configuration.apps_print_url + 'apps.json';
            scope.printApps = [];
            scope.printModels = {};
            scope.modelMapSize = {};
            $http({
              method: 'GET',
              url: getAppRequest,
              headers: {
                'X-CSRFToken': configService.csrfToken
              }
            }).then(function successCallback(response) {
              for (var i = 0; i < response.data.length; i++) {
                if (response.data[i] != 'default' && response.data[i] != 'miniature') {
                  scope.printApps.push(response.data[i]);
                }
              }
              scope.selectedApp = scope.printApps[0];
              getModels();
            }, function errorCallback(response) {
              dialogService.error($translate.instant('Error'), $translate.instant('Unable to connect print server'));
            });
            scope.appsDropboxItemSelected = function(item) {
              scope.selectedApp = item;
              getModels();
            };
            scope.modelsDropboxItemSelected = function(item) {
              scope.selectedModel = item;
              dpiCapabilities();
            };
            scope.resolutionsDropboxItemSelected = function(item) {
              scope.selectedResolution = item;
            };

            function getModels() {
              scope.printModelsName = [];
              $http({
                method: 'GET',
                url: configService.configuration.apps_print_url + scope.selectedApp + '/capabilities.json',
                headers: {
                  'X-CSRFToken': configService.csrfToken
                }
              }).then(function successCallback(response) {
                scope.printModels[scope.selectedApp] = response.data.layouts;
                for (var i = 0; i < scope.printModels[scope.selectedApp].length; i++) {
                  scope.printModelsName.push(scope.printModels[scope.selectedApp][i].name);
                }
                scope.selectedModel = scope.printModelsName[0];
                dpiCapabilities();
              }, function errorCallback(response) {
                dialogService.error($translate.instant('Error'), $translate.instant('Unable to connect print server'));
              });
            }
            function dpiCapabilities() {
              scope.printResolutions = [];
              for (var i = 0; i < scope.printModels[scope.selectedApp].length; i++) {
                if (scope.printModels[scope.selectedApp][i].name === scope.selectedModel) {
                  for (var j = 0; j < scope.printModels[scope.selectedApp][i].attributes.length; j++) {
                    if (scope.printModels[scope.selectedApp][i].attributes[j].name === 'map') {
                      scope.printResolutions = scope.printModels[scope.selectedApp][i].attributes[j].clientInfo.dpiSuggestions;
                      scope.modelMapSize['height'] = scope.printModels[scope.selectedApp][i].attributes[j].clientInfo.height;
                      scope.modelMapSize['width'] = scope.printModels[scope.selectedApp][i].attributes[j].clientInfo.width;
                      drawBox();
                    }
                  }
                }
              }
              scope.selectedResolution = scope.printResolutions[0];
            }

            function drawBox() {
              if ($('#print-extent').length > 0) {
                $('#print-extent').remove();
                $('#map').append('<div id="print-extent"></div>');
                if (($('#map').height() - 40) < scope.modelMapSize['height'] || ($('#map').width() - 360) < scope.modelMapSize['width']) {
                  scope.reducedBox = 1.3;
                } else {
                  scope.reducedBox = 1;
                }
                $('#print-extent').css({
                  'height': scope.modelMapSize['height'] / scope.reducedBox,
                  'width': scope.modelMapSize['width'] / scope.reducedBox,
                  'margin-top': ($('#map').height() - scope.modelMapSize['height'] / scope.reducedBox) / 2,
                  'margin-left': 160 + ($('#map').width() - scope.modelMapSize['width'] / scope.reducedBox) / 2
                });
              }
            }

            function disableUI(bool) {
              pulldownService.pdfLoading = bool;
              $('#title-text').attr('disabled', bool);
              $('#commentary-text').attr('disabled', bool);
              $('#submit-print').attr('disabled', bool);
              $('#modelsDropMenu').attr('disabled', bool);
              $('#applicationsDropMenu').attr('disabled', bool);
              $('#resolutionsDropMenu').attr('disabled', bool);
              if (bool === true) {
                $('#print-icon').hide('fast');
              } else {
                $('#print-icon').show('fast');
              }
            }

            function downloadWhenReady(startTime, data) {
              if ((new Date().getTime() - startTime) > configService.configuration.print_timeout) {
                disableUI(false);
                dialogService.error($translate.instant('Timeout'), $translate.instant('timeout generating the pdf'));
              } else {
                $timeout(function() {
                  $.getJSON(configService.configuration.map_site_domain + data.statusURL, function(statusData) {
                    if (!statusData.done) {
                      downloadWhenReady(startTime, data);
                    } else {
                      disableUI(false);
                      window.location = configService.configuration.map_site_domain + statusData.downloadURL;
                    }
                  }, function error(data) {
                    dialogService.error($translate.instant('Error'), $translate.instant('Unable to generate pdf'));
                  });
                }, 500);
              }
            }

            function generateDataRequest(currentLayers) {
              var requestData = {
                attributes: {
                  titleAtt: $('#title-text').val(),
                  authorAtt: configService.configuration.username,
                  subjectAtt: configService.configuration.map_domains,
                  creatorAtt: '',
                  keywordsAtt: configService.configuration.map_keywords,
                  map: {
                    center: mapService.map.getCoordinateFromPixel([
                      $('#print-extent').offset().left + ($('#print-extent').width() / 2),
                      $('#print-extent').offset().top + ($('#print-extent').height() / 2)
                    ]),
                    dpi: scope.selectedResolution,
                    layers: [],
                    projection: mapService.map.getView().getProjection().getCode(),
                    rotation: 0,
                    scale: ((72 / 0.0254) * mapService.map.getView().getProjection().getMetersPerUnit() * mapService.map.getView().getResolution()) / scope.reducedBox
                  },
                  legend: {
                    classes: []
                  },
                  titre: $('#title-text').val(),
                  commentaire: $('#commentary-text').val()
                },
                layout: scope.selectedModel
              };
              for (var i = 0; i < currentLayers.length; i++) {
                if (currentLayers[i].getVisible()) {
                  requestData.attributes.map.layers.unshift({
                    baseURL: currentLayers[i].values_.source.coordKeyPrefix_,
                    customParams: {TRANSPARENT: true},
                    imageFormat: 'image/png',
                    layers: [currentLayers[i].values_.source.params_.LAYERS],
                    opacity: currentLayers[i].values_.opacity,
                    serverType: currentLayers[i].values_.source.serverType_,
                    type: 'WMS'
                  });
                }
              }
              if (scope.selectedModel.split(' ')[2] === 'avec') {
                for (var j = 0; j < currentLayers.length; j++) {
                  if (currentLayers[j].getVisible()) {
                    requestData.attributes.legend.classes.unshift({
                      name: '',
                      classes: [{
                        name: currentLayers[j].values_.metadata.title || currentLayers[j].values_.source.params_.LAYERS,
                        icons: [currentLayers[j].values_.source.coordKeyPrefix_ + '?version=1.3.0&service=WMS&request=GetLegendGraphic&sld_version=1.1.0&layer=' + currentLayers[j].values_.source.params_.LAYERS + '&format=image/png']
                      }]
                    });
                  }
                }
              }
              return requestData;
            }

            scope.submitPrintForm = function() {
              var startTime = new Date().getTime();
              // Request initial data
              var data = generateDataRequest(mapService.map.getLayers().array_);
              $http({
                method: 'POST',
                url: configService.configuration.apps_print_url + scope.selectedApp + '/report.pdf',
                data: data,
                headers: {
                  'X-CSRFToken': configService.csrfToken
                }
              }).then(function successCallback(response) {
                disableUI(true);
                downloadWhenReady(startTime, response.data);
              }, function errorCallback(response) {
                disableUI(false);
                dialogService.error($translate.instant('Error'), $translate.instant('Unable to connect print server'));
              });
            };
          }
        };
      });
}());
