(function() {
  var module = angular.module('loom_print_service', []);

  module.provider('printService', function() {
    this.startTime = null;
  });

}());
