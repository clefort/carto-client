Proc�dure
Compilation MapLoom

    Se connecter sur la VM FRPARVM48371648.corp.capgemini.com en root/Tsunami7
    Aller dans le r�pertoire MapLoom cd /developpement/MapLoom
    R�cup�rer les sources � jour git pull
    Compiler MapLoom grunt

Installation Django-Maploom

    Aller dans le r�pertoire django-maploom cd /developpement/django-maploom
    R�cup�rer les sources � jour git pull
    Faire le build deploy/build.sh dev
        1�re question : yes
        2�me question : no
    installer MapLoom python setup.py install

Packaging pour livraison

Une fois la modification valid�e, il faut commiter django-maploom depuis le serveur afin que les fichiers mis-�-jour soit int�grer dans la version.

Faire la proc�dure Compilation MapLoom

Dans django-maploom, lancer un build deploy/build.sh dev

    1�re question : yes

    2�me question : yes
        Cr�er un tag portant le num�ro de la version sur le master de MapLoom
        Cr�er un tag portant le num�ro de la version sur le master de django-maploom
        T�l�charger le tag de django-maploom en tar.gz
        Le mettre dans le r�pertoire de livraison GeoNode
