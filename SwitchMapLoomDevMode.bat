set VIRTUAL_ENV_PATH=D:\Projets\AELB\Workspace\VirtualEnv
set WORKSPACE_PATH=D:\Projets\AELB\Workspace\MapLoom

set MAPLOOM_STATIC_PATH=%VIRTUAL_ENV_PATH%\Lib\site-packages\django_maploom-1.7.0-py2.7.egg\maploom\static\maploom
set MAPLOOM_TEMPLATE_PATH=%VIRTUAL_ENV_PATH%\Lib\site-packages\django_maploom-1.7.0-py2.7.egg\maploom\templates\maps\maploom.html

set WS_STATIC_PATH=%WORKSPACE_PATH%\build
set WS_TEMPLATE_PATH=%WORKSPACE_PATH%\build\maploom.html

del /F /Q /S %MAPLOOM_STATIC_PATH%
del /F /Q /S %MAPLOOM_TEMPLATE_PATH%

mklink %MAPLOOM_TEMPLATE_PATH% %WS_TEMPLATE_PATH% 
mklink /D  %VIRTUAL_ENV_PATH%\Lib\site-packages\django_maploom-1.7.0-py2.7.egg\maploom\static\maploom %WS_STATIC_PATH%