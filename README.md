Cart'O

Dans le cadre du projet P0026b relatif à la refonte du SIG de l’Agence, il a été décidé de mettre en œuvre une infrastructure de données spatiales (IDS) qui s’appuie sur la solution libre et open source GeoNode (geonode.org). Différents développements ont été réalisés sur des composants de cette IDS afin de corriger des dysfonctionnements et de s’adapter aux attentes de l’Agence. Toutefois, certaines de ces contributions apportent des améliorations ou corrections qui peuvent être bénéfiques à l’ensemble de la communauté GeoNode, c’est pourquoi il a été décidé de les reverser à cette dernière en licence ouverte.

Le client cartographique de GeoNode, basé sur GeoExplorer a été remplacé par une évolution de MapLoom, initialement développé pour GeoShape, un fork de GeoNode.
(https://github.com/ROGUE-JCTD/MapLoom).

Cette contribution au projet MapLoom (ajout du module d’impression MapFish Print) est portée par l’Agence de l’eau Loire-Bretagne (http://www.eau-loire-bretagne.fr) dans le cadre de son projet de refonte SIG qui s’articule autour de la mise en œuvre de l’Infrastructure de Données Spatiales GeoNode.
Les développements informatiques ont été réalisés pour l’Agence de l’eau Loire-Bretagne par l’entreprise de services du numérique Capgemini (http://www.fr.capgemini.com/).
L’Agence de l’eau, établissement public français, s’engage depuis plus de 50 ans aux côtés des élus et des usagers de l’eau pour la qualité de l’eau et des milieux aquatiques.

Cart'O est distribué en licence GPL V3.

MapLoom License
===============

The MIT License (MIT)

Copyright (c) 2013-2014 LMN Solutions, LLC
Copyright (c) 2014 Noblis NSP
Copyright (c) 2015-2016 Prominent Edge, LLC.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.s/>.

	